# [honzik-keyboard-layout](https://gitlab.com/stranskyjan/honzik-keyboard-layout)
Hybrid English-Czech keyboard layout for [X Keyboard Extension](https://www.x.org/wiki/XKB/) (used e.g. in [Ubuntu](https://www.ubuntu.com/) Linux)

## Overview
A user-defined custom Layout, based on basic (qwerty) US English layout,
adding "special Czech" characters as `AltGr` alternatives.

For example:
- `AltGr+a` results in `á`
- `AltGr+Shift+t` results in `Ť`
- `AltGr+Shift+~` results in `°`
- etc.

Caps lock works as expected.

There may be multiple ways how to write one "special" character.
E.g. `č` may be written as `AltGr+c` or `AltGr+4`
(the latter without `AlrGr` would be standard Czech layout).

See the complete layout for illustration:

![layout](layout.png)

### Naming
For the sake of simplicity, the layout is named "custom" or "A user-defined custom Layout", as the Ubuntu 22.04 predefined custom file.

### Compatibility
Tested on [Ubuntu](https://www.ubuntu.com/) 22.04

## Installation
```sh
[sudo] make install
```
(runs `cp src/custom /usr/share/X11/xkb/symbols` internally)

Maybe also one of:
- `[sudo] make clean-cache`
- `[sudo] dpkg-reconfigure-xkb-data`
- log out, log in
- reboot
- ... ?

Then select "custom" or "A user-defined custom Layout" in Keyboard settings.

## Contribution
#### Merge Requests
Are welcome

#### Bug reporting
In case of any question or problem, please leave an issue at the [GitLab page of the project](https://gitlab.com/stranskyjan/honzik-keyboard-layout/-/issues).

#### Contributors
- [Jan Stránský](https://gitlab.com/stranskyjan)

## License
[MIT](https://en.wikipedia.org/wiki/MIT_License)
