XKB = /usr/share/X11/xkb

install:
	cp src/custom $(XKB)/symbols

clean-cache:
	rm -f /var/lib/xkb/*.xkm

dpkg-reconfigure-xkb-data:
	dpkg-reconfigure xkb-data
